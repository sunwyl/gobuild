#!/bin/bash

set -e

User="sunwyl"
echo "User :$User"
Password="Sun.qwe123"
docker login --username ${User} --password ${Password}

WORKING_PATH=`pwd`
BUILD_ID="1.17.5-alpine3.15"
version=${BUILD_ID}-$(date "+%Y%m%d")
echo "version :$version"

cd ${WORKING_PATH}

echo "WORKING PATH: ${WORKING_PATH}"

ls ${WORKING_PATH}

GOLANG_IMAGE="sunwyl/golang:${version}"

echo "golang image: ${GOLANG_IMAGE}"

docker build . \
-f ./Dockerfile \
-t ${GOLANG_IMAGE}

docker push ${GOLANG_IMAGE}