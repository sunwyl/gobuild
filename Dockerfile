FROM golang:1.17.5-alpine3.15 as builder

LABEL maintainer="wyl"

COPY . .

WORKDIR /app/service

ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct \
    CGO_ENABLED=1

